<?php

/**
 * Implements hook_default_message_type().
 */
function commerce_shipment_message_default_message_type() {
  $items['commerce_shipment_tracking_number_notification'] = entity_import('message_type', '{
    "name" : "commerce_shipment_tracking_number_notification",
    "description" : "Commerce Shipment: tracking number notification",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "Shipment notification for order @{message:message-commerce-order:order-number} at [site:name]",
          "format" : null,
          "safe_value" : "\u003Cp\u003EShipment notification for order @{message:message-commerce-order:order-number} at [site:name]\u003C\/p\u003E\n"
        },
        {
          "value" : "Your order @{message:message-commerce-order:order-number} at [site:name] has new shipments.\r\n\r\n!order-tracking-numbers",
          "format" : null,
          "safe_value" : "\u003Cp\u003EYour order @{message:message-commerce-order:order-number} at [site:name] has new shipments.\u003C\/p\u003E\n\u003Cp\u003E!order-tracking-numbers\u003C\/p\u003E\n"
        }
      ]
    },
    "rdf_mapping" : []
  }');

  return $items;
}


/**
 * Implements hook_commerce_message_message_fields_alter().
 *
 * @see commerce_message_message_field_refresh
 * @param $fields
 */
function commerce_shipment_message_commerce_message_message_fields_alter(&$fields) {
  // Add an instance of message_commerce_order to our notification type.
  $fields['message_commerce_order']['instances'][0]['bundles'][] = 'commerce_shipment_tracking_number_notification';
}
