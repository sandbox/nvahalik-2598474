<?php

module_load_include('inc', 'commerce_shipment_message', 'commerce_shipment_message.message');

/**
 * Implements hook_message_presave().
 *
 * Adds a token to the message so that we can show the tracking numbers.
 * @param $message
 *   The Message entity.
 */
function commerce_shipment_message_message_presave($message) {
  if (!empty($message->mid)) {
    return;
  }

  $message->arguments['!order-tracking-numbers'] = array(
    'callback' => 'commerce_shipment_message_tracking_numbers',
    'pass message' => TRUE,
  );
}

/**
 * Message callback; Show order summary.
 *
 * @param $message
 *   The Message entity.
 */
function commerce_shipment_message_tracking_numbers(Message $message) {
  $wrapper = entity_metadata_wrapper('message', $message);
  $id = $wrapper->message_commerce_order->getIdentifier();

  if ($id) {
    $view = views_get_view('commerce_shipment_message_order_summary');
    $view->set_arguments(array($id));
    $view->hide_admin_links = TRUE;
    return $view->preview();
  }

  return '';
}

/**
 * Implements hook_views_api().
 */
function commerce_shipment_message_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'commerce_shipment_message') . '/includes/views',
  );
}

/**
 * Implements hook_FORM_ID_form_alter().
 *
 * Add our submit handlers so that we can take action when the IEF form gets
 * submitted and the order is saved.
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function commerce_shipment_message_form_commerce_order_ui_order_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form_state['inline_entity_form'])) {
    // Try to find the main submit button in the most common places.
    $submit_element = NULL;
    if (!empty($form['submit'])) {
      $submit_element = &$form['submit'];
    }
    elseif (!empty($form['actions']['submit'])) {
      $submit_element = &$form['actions']['submit'];
    }

    if ($submit_element) {
      $submit = array_merge(array('commerce_shipment_message_order_form_submit'), $form['#submit']);
      if (!empty($submit_element['#submit'])) {
        $submit = array_merge($submit, $submit_element['#submit']);
        $submit[] = 'commerce_shipment_message_order_form_post_submit';
        // $form['#submit'] and $submit_element['#submit'] might have matching
        // callbacks, resulting in duplicates and double processing.
        $submit_element['#submit'] = array_unique($submit);
      }
      else {
        $submit[] = 'commerce_shipment_message_order_form_post_submit';
        $submit_element['#submit'] = $submit;
      }
    }
  }
}

/**
 * Take a look at the order (before it is saved) to compare the existing values
 * to values in the DB. This way we can tell if the data has changed.
 *
 * @param $form
 * @param $form_state
 */
function commerce_shipment_message_order_form_submit($form, &$form_state) {
  $needs_tracking_update = FALSE;

  $ief_form_id = $form['commerce_shipments'][LANGUAGE_NONE]['#ief_id'];
  $ief_form_state = &$form_state['inline_entity_form'][$ief_form_id];

  foreach ($ief_form_state['entities'] as $id => $data) {
    if (!$data['needs_save']) {
      continue;
    }

    $entity = $data['entity'];
    $new_wrapper = entity_metadata_wrapper('commerce_shipment', $entity);

    if (empty($entity->shipment_id)) {
      if ($new_wrapper->commerce_shipment_tracking_num->value() != '') {
        $needs_tracking_update = TRUE;
        break;
      }
    }
    else {
      $shipment = commerce_shipment_load($entity->shipment_id);
      $original_wrapper = entity_metadata_wrapper('commerce_shipment', $shipment);
      if ($original_wrapper->commerce_shipment_tracking_num->value() != $new_wrapper->commerce_shipment_tracking_num->value()) {
        $needs_tracking_update = TRUE;
        break;
      }
    }
  }

  // Save our form state so that the latter handler will pick it up.
  $form_state['needs_tracking_update'] = $needs_tracking_update;
}

/**
 * Form submit handler that checks to see if the order has a tracking update.
 * This has to fire last since we need the order and the shipment information to
 * be saved before we can properly fire off the notification.
 *
 * @param $form
 * @param $form_state
 */
function commerce_shipment_message_order_form_post_submit($form, &$form_state) {
  if ($form_state['needs_tracking_update']) {
    rules_invoke_event_by_args('commerce_shipment_message_tracking_changed', array('commerce_order' => $form_state['commerce_order']));
  }
}
