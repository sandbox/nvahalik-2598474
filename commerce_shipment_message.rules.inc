<?php

/**
 * Implements hook_rules_event_info().
 */
function commerce_shipment_message_rules_event_info() {
  $events['commerce_shipment_message_tracking_changed'] = array(
    'label' => t('Shipment tracking number(s) updated'),
    'group' => t('Commerce Order'),
    'variables' => array(
      'commerce_order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
        'description' => t('The order whose tracking information has been udpated'),
      ),
    ),
  );

  return $events;
}
