<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_shipment_message_default_rules_configuration() {
  $rules['commerce_message_commerce_order_message_shipment_notification'] = entity_import('rules_config', '{ "commerce_message_commerce_order_message_shipment_notification" : {
      "LABEL" : "Commerce order message: shipment notification e-mail",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "3",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "message_notify", "entity" ],
      "ON" : { "commerce_shipment_message_tracking_changed" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "commerce_shipments" } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "commerce_shipment_tracking_number_notification",
              "param_user" : [ "commerce-order:owner" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:message-commerce-order" ],
            "value" : [ "commerce-order" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : 1 } },
        { "message_notify_process" : {
            "message" : [ "entity-created" ],
            "save_on_fail" : 0,
            "save_on_success" : 0
          }
        }
      ]
    }
  }');

  return $rules;
}
